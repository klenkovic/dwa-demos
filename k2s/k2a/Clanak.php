<?php


class Clanak {
    
    public $id;
    public $naslov;
    public $sadrzaj;
    public $autor;
    public $datum;
    public $jeObjavljeno;

    public function __construct($id="") {
        if($id!=""){
            $baza = $this->citajDatoteku();
            //Clanak::debug($baza);
            
            foreach($baza as $clanak){
                if($clanak[0] == $id){
                    $this->id = $clanak[0];
                    $this->naslov = $clanak[1];
                    $this->sadrzaj = $clanak[2];
                    $this->autor = $clanak[3];
                    $this->datum = $clanak[4];
                    $this->jeObjavljeno = $clanak[5];
                    break;
                    //return;
                }
            }
            
        }
    }
    
    public function objavi() {
        $baza = $this->citajDatoteku();
        $novaBaza = array();
        foreach($baza as $clanak){
            if($clanak[0] == $this->id){
                $clanak[5] = 1;
                $this->jeObjavljeno = 1;
            }
            array_push($novaBaza, $clanak);
        }
        $this->azuriranjeDatoteke($novaBaza);
    }
    
    public function sakrij() {
        $baza = $this->citajDatoteku();
        $novaBaza = array();
        foreach($baza as $clanak){
            if($clanak[0] == $this->id){
                $clanak[5] = 0;
                $this->jeObjavljeno = 0;
            }
            array_push($novaBaza, $clanak);
        }
        $this->azuriranjeDatoteke($novaBaza);
    }
    
    public function Link_Na_Clanak($id) {
        
        $baza = $this->citajDatoteku();
         
        foreach($baza as $clanak){
            if($clanak[0] == $id){
                echo "<a href=\"z2.php?id=".$clanak[0]."\">".$clanak[1]."</a>";
            }
        }
        
        
        //echo "<a href=\"z2.php?id=".$this->id."\">".$this->naslov."</a>";
    }
    
    public function prikaz($id) {
        $baza = $this->citajDatoteku();
         
        foreach($baza as $clanak){
            if($clanak[0] == $id){
                $zaIspis = "";
                $zaIspis .= "<table>";
                $zaIspis .= "<tr><td>ID</td><td>".$clanak[0]."</td></tr>";
                $zaIspis .= "<tr><td>Naslov</td><td>".$clanak[1]."</td></tr>";
                $zaIspis .= "<tr><td>Sadržaj</td><td>".$clanak[2]."</td></tr>";
                $zaIspis .= "<tr><td>Autor</td><td>".$clanak[3]."</td></tr>";
                $zaIspis .= "<tr><td>Datum</td><td>".$clanak[4]."</td></tr>";
                $zaIspis .= "<tr><td>Objavljeno</td><td>".$clanak[5]."</td></tr>";
                $zaIspis .= "</table>";
                echo $zaIspis;
                break;
            }
        }
    }
    
    public function citajDatoteku(){
        
        $dat = fopen("clanci.txt", "r");
        $baza = array();
        
        while(!feof($dat)){
            array_push($baza, explode("-",trim(fgets($dat))));
        }
        array_pop($baza);
        
        fclose($dat);
        return $baza;
    }
    
    public function azuriranjeDatoteke($zaUpis){
        $dat = fopen("clanci.txt", "w");
        foreach($zaUpis as $clanak){
            fwrite($dat, implode("-", $clanak) . "\n");
        }
        fclose($dat);
        return;
    }
    
    public static function debug($a){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }
    
}


?>