<?php

require_once 'Clanak.php';

class ClanakManager {
    
    public static function prikazSvihClanaka() {
        $baza = Clanak::citajDatoteku();
        $ids = array();
        
        foreach ($baza as $clanak){
            array_push($ids , $clanak[0]);
        }
        
        foreach ($ids as $id){
            $clanak = new Clanak($id);
            $clanak->Link_Na_Clanak($id);
            echo "<br />";
        }
    }
    
    public static function unosNovog() {
        $html = '
        <form method="post">
            <table>
                <tr>
                    <td>ID</td>
                    <td><input type="text" name="id" /></td>
                </tr>
                <tr>
                    <td>Naslov</td>
                    <td><input type="text" name="naslov" /></td>
                </tr>
                <tr>
                    <td>Sadržaj</td>
                    <td><input type="text" name="sadrzaj" /></td>
                </tr>
                <tr>
                    <td>Autor</td>
                    <td><input type="text" name="autor" /></td>
                </tr>
                <tr>
                    <td>Datum</td>
                    <td><input type="text" name="datum" /></td>
                </tr>
                <tr>
                    <td>Objavljeno</td>
                    <td><input type="text" name="objavljeno" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Unesi novi" /></td>
                </tr>
            </table>
        </form>';
        echo $html;
     
    }
    
    public static function SpremiClanak() {
        $zaSpremiti = array($_POST['id'], $_POST['naslov'], $_POST['sadrzaj'], $_POST['autor'], $_POST['datum'], $_POST['objavljeno']);
        $baza = Clanak::citajDatoteku();
        array_push($baza, $zaSpremiti);
        Clanak::azuriranjeDatoteke($baza);
        
    }
    
}

?>